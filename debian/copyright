Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: CSFML
Source: https://github.com/SFML/CSFML/releases

Files: *
Copyright: 2007-2024 Laurent Gomila <laurent.gom@gmail.com>
License: Zlib

Files: debian/*
Copyright: 2008,2009 Christoph Egger <debian@christoph-egger.org>
           2013-2025 James Cowgill <jcowgill@debian.org>
License: Zlib

Files: examples/doodle_pop.ogg
Copyright: MrZeusTheCoder
License: Unlicense
Comment: https://github.com/MrZeusTheCoder/public-domain

Files: examples/tuffy.ttf
Copyright: 2004-2011, Thatcher Ulrich, Karoly Barta and Michael Everson
License: public-domain-tuffy
 We, the copyright holders of this work, hereby release it into the
 public domain. This applies worldwide.
 .
 In case this is not legally possible,
 .
 We grant any entity the right to use this work for any purpose, without
 any conditions, unless such conditions are required by law.
 .
 Thatcher Ulrich <tu@tulrich.com> http://tulrich.com
 Karoly Barta
 Michael Evans http://www.evertype.com

License: Unlicense
 This is free and unencumbered software released into the public domain.
 .
 Anyone is free to copy, modify, publish, use, compile, sell, or
 distribute this software, either in source code form or as a compiled
 binary, for any purpose, commercial or non-commercial, and by any
 means.
 .
 In jurisdictions that recognize copyright laws, the author or authors
 of this software dedicate any and all copyright interest in the
 software to the public domain. We make this dedication for the benefit
 of the public at large and to the detriment of our heirs and
 successors. We intend this dedication to be an overt act of
 relinquishment in perpetuity of all present and future rights to this
 software under copyright law.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 .
 For more information, please refer to <https://unlicense.org>

License: Zlib
 This software is provided 'as-is', without any express or implied warranty.
 In no event will the authors be held liable for any damages arising from
 the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not claim
    that you wrote the original software. If you use this software in a product,
    an acknowledgment in the product documentation would be appreciated but is
    not required.
 .
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source distribution.
