#include <stdio.h>
#include <CSFML/Audio.h>
#include <CSFML/Graphics.h>
#include <CSFML/Network.h>
#include <CSFML/OpenGL.h>
#include <CSFML/System.h>
#include <CSFML/Window.h>

int main(void)
{
    // The build test doesn't check any graphics since that would require a
    // display server. We just test some basic Network / System functionality.
    // However when building we can still include the other headers to ensure
    // they compile.

    char buf[16];
    sfIpAddress local_addr;

    // Print local IP
    local_addr = sfIpAddress_getLocalAddress();
    sfIpAddress_toString(local_addr, buf);
    printf("Local IP = %s\n", buf);

    // Call some time functions from sfml-system
    if (sfTime_asMilliseconds(sfSeconds(5)) != 5000)
        return 1;

    return 0;
}
